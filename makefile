OBJS = ptquiz.o main.o
CC = g++
DEBUG = -g
CFLAGS = -c $(DEBUG)
LFLAGS = $(DEBUG)

default: PeriodicTableQuiz

ptquiz.o: ptquiz.h ptquiz.cpp
	$(CC) $(CFLAGS) ptquiz.cpp

main.o: main.cpp ptquiz.h
	$(CC) $(CFLAGS) main.cpp

PeriodicTableQuiz : $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o PeriodicTableQuiz

clean: 
	\rm *.o PeriodicTableQuiz
