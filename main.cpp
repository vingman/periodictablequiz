#include "ptquiz.h"

int main(int argc, char* argv[])
{
   int right;
   double grade;
   Elements elements;
   string exepath;
   ifstream text;
   ofstream newHighScores;
   map<double, string> scores;
   map<double, string>::iterator it;
   string today, hsfile;
   int i;
   char ans;
   
   exepath = argv[0];
   exepath = exepath.substr(0,exepath.rfind('/'));
   
   elements.genElements(118, exepath);
   
   /* quiz user */
   right = elements.quizUser(118);
   
   /* output grade */
   grade = (right/118.0)*100;
   printf("You got %.2f", grade);
   cout << "% correct!\n";
   
   /* high scores */
   cout << "Would you like to see the high scores? (y/n) ";
   cin >> ans;
   if (ans != 'n' && ans != 'N'){
      string filename;
      time_t t = time(NULL);
      tm *date = localtime(&t);
      today = to_string(1 + date->tm_mon) + '/';
      today = today + to_string(date->tm_mday) + '/';
      today = today + to_string(1900 + date->tm_year);
      //error check filepath
      hsfile = "highScores.txt";
      i = 0;
      text.open(hsfile.c_str());
      while (text.fail()){
         // try to find it using argv[0] (stored in exepath by main)
         if (i == 0){
            filename = exepath + "/highScores.txt";
            text.clear();
            text.open(filename.c_str());
            if (!text.fail()){
               hsfile = filename;
               break;
            }
            i = 1; // this way it won't keep trying after failing
         }
         // that didn't work, so ask the user
         cerr << "failed to open file! " << endl;
         cerr << "perhaps the path to the high scores file is wrong..." << endl;
         cout << "can you tell me where highScores.txt is? (enter file name or enter -1 to cancel) \n";
         cout << ">> ";
         cin >> filename;
         if (filename == "-1"){
            i = -1;
            break;
         }
         text.clear();
         text.open(filename.c_str());
      }
      // put high scores into map and print
      if (i != -1){
         double gg; string tt;
         while(text >> gg >> tt){
            scores[gg] = tt;
         }
         printf("\n\nScore (%%) | Date (M/D/Y)\n");
         cout << string(24,'-') << endl;
         for (it = scores.begin(); it != scores.end(); it++){
            printf("%-10.2f| %s\n", it->first, it->second.c_str());
         }
      }
      text.close();
      // output new high score
      newHighScores.open(hsfile.c_str(),ofstream::app);
      newHighScores << grade << " " << today << endl;
      newHighScores.close();
   }
   return 0;
}
