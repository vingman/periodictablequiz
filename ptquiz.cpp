#include "ptquiz.h"

void Elements::genElements(int n, string exepath){
   int num, i;
   ifstream text;
   string sym, elm;

   i = 0;
   text.open(FILEPATH.c_str());
   while (text.fail()){
      string filename;
      // try to find it using argv[0] (stored in exepath by main)
      if (i == 0){
         filename = exepath + "/elements.txt";
         text.clear();
         text.open(filename.c_str());
         if (!text.fail()){ 
            break;
         }
         i = 1; // this way it won't keep trying after failing
      }
      // that didn't work, so ask the user
      cerr << "failed to open file! " << endl; 
      cerr << "perhaps the path to the element file is wrong..." << endl;
      cout << "can you tell me where elements.txt is? (enter file name or enter -1 to abort) \n";
      cout << ">> ";
      cin >> filename;
      if (filename == "-1") exit(1);
      text.clear();
      text.open(filename.c_str());
   }

   symbols.resize(n);
   names.resize(n);
   while (text >> sym >> elm >> num){
      symbols[num-1] = sym;
      names[num-1] = elm;
   }
   /* 
   could later put something here to add new elements so you don't
   have to hand edit the text file... unless you misspell it 
    */
   text.close();

}

int Elements::quizUser(int n){
   vector<int> skipped;
   string sym, elm, answer;
   int num, i, ans, right = 0;

   // at first, all cells are just atomic numbers
   correct.resize(n);
   numsUsed.resize(n);
   for (i = 1; i <= n; i++){
      string temp = to_string(i);
      correct[i-1] = REG + temp + REG;
      numsUsed[i-1] = i;
   }
   // generate random element list
   randomOrder(numsUsed);

   cout << string(25, '\n');
   printTable(correct);

   // cycle through element list and quiz user on what the atomic number is
   for (i = 0; i < numsUsed.size(); i++){
      num = numsUsed[i];
      sym = symbols[num - 1];
      elm = names[num - 1];

      // clear screen and print current table
      printf("enter then atomic number of %s (%s)\n", elm.c_str(), sym.c_str());
      cout << "(enter 0 to skip)\n";

      // error check input
      do{
         cin >> answer;
         try {
            ans = stod(answer);
         }catch (const invalid_argument& ai){ 
            ans = -1; 
         }

         if (ans < 0 || ans > n){
            cerr << "error: must be a number between 0 and " << n << endl;
         }
      } while (ans < 0 || ans > n);

      // check answer
      if (ans == 0){
         skipped.push_back(num);
      }else if (ans == num){
         right++;
         correct[num - 1] = RIGHT + sym + REG;
      }else{
         printf("incorrect, %s has the atomic number %d \n", elm.c_str(), num);
         correct[num - 1] = WRONG + sym + REG;
         pause();
      }
      //deal with skipped elements
      if (!skipped.empty() && i == numsUsed.size() - 1){
         numsUsed.resize(skipped.size());
         numsUsed = skipped; 
         skipped.clear();
         randomOrder(numsUsed);
         i = -1; // restarting the big for loop, gets incremented at beginning
      }
      cout << string(25, '\n');
      printTable(correct);
   }
   return right;
}

void Elements::randomOrder(vector<int> &list){
   int i, j, num;

   srand(time(0));
   for (i = 0; i < list.size(); i++){
      num = (rand() % list.size());
      j = list[num];
      list[num] = list[i];
      list[i] = j;
   }
}

void Elements::printTable(vector<string> correct){
   //the strings containing the element symbols must already include
   //the escape sequences for the colors
   int i;
   char buffer[100];
   string top(3,'-');
   string notop(3,' ');

//{{ ROW 1
   // row 1 top
   sprintf(buffer," %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s", top.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),top.c_str());
   string row1top; row1top = buffer;
   
   // row 1 space
   string row1space;
   row1space = notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop;
   
   //row 1
   cout << REG << row1top << notop << " " << endl;
   printf("|%23s|%s|%23s|\n", correct[0].c_str(), row1space.c_str(), correct[1].c_str());
//}}

//{{ ROW 2 AND ROW 3
   // top of row 2 and 3
   sprintf(buffer," %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",top.c_str(),top.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),notop.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str());
   string row2top; row2top = buffer;

   // row 2 and 3 space
   string row2space;
   row2space = notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop+' '+notop;

   //row 2
   cout << row2top << endl;
   printf("|%23s|%23s|%s", correct[2].c_str(), correct[3].c_str(), row2space.c_str());
   for (i = 4; i < 10; i++){
      printf("|%23s", correct[i].c_str());
   }
   printf("|\n");

   //row 3
   cout << row2top << endl;
   printf("|%23s|%23s|%s", correct[10].c_str(), correct[11].c_str(), row2space.c_str());
   for (i = 12; i < 18; i++){
      printf("|%23s", correct[i].c_str());
   }
   printf("|\n");
//}}

//{{ ROW 4 AND ROW 5
   //top of row 4 and 5
   sprintf(buffer," %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",top.c_str(),top.c_str(),notop.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str());
   string row4top; row4top = buffer;

   //row 4 and 5 space
   string row4space; 
   row4space = notop;

   //row 4
   cout << row4top << endl;
   printf("|%23s|%23s|%s", correct[18].c_str(), correct[19].c_str(), row4space.c_str());
   for (i = 20; i < 36; i++){
      printf("|%23s", correct[i].c_str());
   }
   printf("|\n");

   //row 5
   cout << row4top << endl;
   printf("|%23s|%23s|%s", correct[36].c_str(), correct[37].c_str(), row4space.c_str());
   for (i = 38; i < 54; i++){
      printf("|%23s", correct[i].c_str());
   }
   printf("|\n");
//}}

//{{ ROW 6 and 7
   //top of row 6 and 7
   sprintf(buffer," %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str());
   string row6top; row6top = buffer;

   //row 6
   cout << row6top << endl;
   printf("|%23s|%23s| * ", correct[54].c_str(), correct[55].c_str());
   for (i = 70; i < 86; i++){
      printf("|%23s", correct[i].c_str());
   }
   printf("|\n");

   //row 7
   cout << row6top << endl;
   printf("|%23s|%23s| # ", correct[86].c_str(), correct[87].c_str());
   for (i = 102; i < 118; i++){
      printf("|%23s", correct[i].c_str());
   }
   printf("|\n%s\n", row6top.c_str());
//}}

//{{ LANTHANIDES AND ACTINIDES
   //lanth and act tops/bottom
   sprintf(buffer," %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",notop.c_str(),notop.c_str(),notop.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),top.c_str(),notop.c_str(),notop.c_str());
   string lanthrow; lanthrow = buffer;
   
   // lanth and act space
   string lanthrowspace;
   lanthrowspace = ' '+notop+' '+notop+' ';

   //row 8
   cout << endl << lanthrow << endl;
   printf("%s * ", lanthrowspace.c_str());
   for (i = 56; i < 70; i++){
      printf("|%23s", correct[i].c_str());
   }
   printf("|\n");

   //row 8
   printf("%s\n", lanthrow.c_str());
   printf("%s # ", lanthrowspace.c_str());
   for (i = 88; i < 102; i++){
      printf("|%23s", correct[i].c_str());
   }
   printf("|\n%s\n", lanthrow.c_str());
//}}
}

void pause(){
   cin.clear();
   cout << "press any key to continue" << endl;
   cin.ignore();
   cin.get();
}
