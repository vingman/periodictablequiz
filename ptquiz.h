#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <stdexcept>
#include <sstream>
#include <cstdio>
#include <map>
using namespace std;

const string FILEPATH = "elements.txt";

const string REG = "\x1b[40;1;37m";   // [ black-bg; bold; white-fg m
const string WRONG = "\x1b[40;1;35m"; // [ black-bg; bold; magenta-fg m
const string RIGHT = "\x1b[40;1;36m"; // [ black-bg; bold; cyan-fg m

class Elements{
private:
    vector<string> symbols;
    vector<string> names;
    vector<string> correct;
    vector<int> numsUsed;
public:
    void genElements(int n, string exepath);
    int quizUser(int n);
    void randomOrder(vector<int> &list);
    void printTable(vector<string> correct);
};

void pause();
